function validateForm() 
{
	var nome = document.forms["monitor"]["nome"].value;
	var ramal = document.forms["monitor"]["ramal"].value;
	var senha = document.forms["monitor"]["senha"].value;
	var lista = document.getElementById("monitorados").length;

	if (ramal == null || ramal == "") {
		alert("O ramal deve ser informado.");
		return false;
	}

	if (isNaN(ramal)) {
		alert("O ramal deve ser numérico.");
		return false;
	}
	
	if (nome == null || nome == "") {
		alert("O nome deve ser informado.");
		return false;
	}

	if (senha == null || senha == "") {
		alert("A senha deve ser informada.");
		return false;
	}

	if (isNaN(senha)) {
		alert("A senha deve ser numérica.");
		return false;
	}

	if (senha.length <4) {
		alert("A senha deve ter 4 dígitos.");
		return false;
	}


	if (lista == null || lista == 0) {
		alert("Algum ramal deve ser monitorado.");
		return false;
	}

	if (check_ramal(ramal) == 0) {
		alert("Ramal inexistente.");
		return false;
	}

	selectAll('monitorados',true)	

	document.forms['monitor'].submit();
	return true;
}

function selectAll(selectBox,selectAll) { 
    // have we been passed an ID 
    if (typeof selectBox == "string") { 
        selectBox = document.getElementById(selectBox);
    } 
    // is the select box a multiple select box? 
    if (selectBox.type == "select-multiple") { 
        for (var i = 0; i < selectBox.options.length; i++) { 
             selectBox.options[i].selected = selectAll; 
						 document.getElementById('ramais_monitorados').value = document.getElementById('ramais_monitorados').value + "," + selectBox.options[i].value
        } 
    }
}

function ConfirmDelete(id) 
{
	var resposta = confirm("Deseja remover esse registro?");

	if (resposta == true) 
	{
		window.location.href = "/delete/"+id; 
	}
}

function check_ramal(ramal)
{

	found=0;

	for (var i=0; i<document.getElementById('ramais').options.length; i++)
	{ 
		if (document.getElementById('ramais').options[i].text == ramal) 
		{ 
			found=1;
			break;
		} 
	}

	if (found==0) 
	{
		for (var i=0; i<document.getElementById('monitorados').options.length; i++)
		{ 
			if (document.getElementById('monitorados').options[i].text == ramal) 
			{ 
				found=1;
				break;
			} 
		}
	}
	return found;
};


function move(tbFrom, tbTo) 
{
	var arrFrom = new Array(); var arrTo = new Array(); 
	var arrLU = new Array();
	var i;
	for (i = 0; i < tbTo.options.length; i++) 
	{
		arrLU[tbTo.options[i].text] = tbTo.options[i].value;
		arrTo[i] = tbTo.options[i].text;
	}
	var fLength = 0;
	var tLength = arrTo.length;
	for(i = 0; i < tbFrom.options.length; i++) 
	{
		arrLU[tbFrom.options[i].text] = tbFrom.options[i].value;
		if (tbFrom.options[i].selected && tbFrom.options[i].value != "") 
		{
			arrTo[tLength] = tbFrom.options[i].text;
			tLength++;
		}
		else 
		{
			arrFrom[fLength] = tbFrom.options[i].text;
			fLength++;
		}
}

tbFrom.length = 0;
tbTo.length = 0;
var ii;

for(ii = 0; ii < arrFrom.length; ii++) 
{
		var no = new Option();
		no.value = arrLU[arrFrom[ii]];
		no.text = arrFrom[ii];
		tbFrom[ii] = no;
}

for(ii = 0; ii < arrTo.length; ii++) 
{
	var no = new Option();
	no.value = arrLU[arrTo[ii]];
	no.text = arrTo[ii];
	tbTo[ii] = no;
}
}
