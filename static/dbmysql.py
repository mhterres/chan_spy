#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		dbmysql.py
		Classe para acesso ao DB MySQL
		2015/02/26
		Marcelo Hartmann Terres <mhterres@mundoopensource.com.br>
"""
import MySQLdb

class DBMysql:

	def __init__(self,config):

		self.description = 'MySQL Database'

		self.conn = MySQLdb.connect(config.dbhost,config.dbuser,config.dbpwd,config.dbname)
		self.config = config

	def delete_monitor(self,monitor_id):

		sql = "DELETE FROM monitores where id=%i;" % int(monitor_id)

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)
		cursor.execute("COMMIT;")
		cursor.close()

	def delete_ramais(self,monitor_id):

		sql = "DELETE FROM ramais where monitor_id=%i;" % int(monitor_id)

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)
		cursor.execute("COMMIT;")
		cursor.close()

	def insert_monitor(self,data):

		sql="INSERT INTO monitores (ramal,nome,senha) VALUES ('%s','%s','%s');" % (data['ramal'],data['nome'],data['senha'])

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)
		cursor.execute("COMMIT;")
		cursor.close()

		sql="SELECT id FROM monitores where ramal='%s';" % data['ramal']

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)

		row=cursor.fetchone()

		monitor_id=row[0]
		ramais=data['ramais']

		cursor.close()

		self.insert_ramais(monitor_id,ramais)

	def update_monitor(self,data):

		sql="UPDATE monitores SET ramal='%s',nome='%s',senha='%s' where id=%i;" % (data['ramal'],data['nome'],data['senha'],int(data['monitor_id']))

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)
		cursor.execute("COMMIT;")
		cursor.close()

		self.delete_ramais(data['monitor_id'])

		self.insert_ramais(int(data['monitor_id']),data['ramais'])

	def insert_ramais(self,monitor_id,ramais):

		for ramal in ramais:

			sql="INSERT INTO ramais (monitor_id,ramal,nome) VALUES (%i,'%s','%s');" %(monitor_id,ramal['ramal'],ramal['nome'])

			if self.config.debug=="1":

				print sql

			cursor = self.conn.cursor()
			cursor.execute(sql)
			cursor.execute("COMMIT;")
			cursor.close()

	def search_monitor(self,monitor_id):

		sql = "SELECT id,ramal,senha,nome from monitores where id=%s;" % monitor_id

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)

		data=[]

		if cursor.rowcount>0:

			row=cursor.fetchone()

			data=dict(monitor_id=int(row[0]),ramal=row[1],senha=row[2],nome=row[3])

		return data

	def search_monitores(self):

		sql = "SELECT id,ramal,senha,nome from monitores;"

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)

		data=[]

		if cursor.rowcount>0:

			row = cursor.fetchone()

			while row is not None:
			
				data.append(dict(monitor_id=row[0],ramal=row[1],senha=row[2],nome=row[3]))
				row = cursor.fetchone()

		return data


	def search_ramais(self,monitor_id):

		sql="SELECT id,monitor_id,ramal,nome FROM ramais where monitor_id=%i;" % monitor_id

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)

		data=[]

		if cursor.rowcount>0:

			row = cursor.fetchone()

			while row is not None:

				data.append(dict(ramal_id=row[0],monitor_id=row[1],ramal=row[2],nome=row[3]))
				row = cursor.fetchone()

		cursor.close()
		return data

	def VerificaRamalCadastrado(self,ramal):

		sql = "SELECT ramal FROM monitores where ramal='%s';" % ramal

		if self.config.debug=="1":

			print sql

		cursor = self.conn.cursor()
		cursor.execute(sql)

		if cursor.rowcount>0 :

			found=True
		else:

			found=False

		cursor.close()

		return found

