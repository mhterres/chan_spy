#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		config.py
		Classe da configuração do sistema
		2015/02/20
		Marcelo Hartmann Terres <mhterres@mundoopensource.com.br>
"""

import os
import sys
import ConfigParser

class Config:

	def __init__(self):

		self.description = 'Configurations'

		configuration = ConfigParser.RawConfigParser()

		pathname = os.path.dirname(sys.argv[0])
		path=os.path.abspath(pathname)
		configuration = ConfigParser.RawConfigParser()
		configuration.read('%s/config_cs/chanspy.conf' % path)

    # geral
		self.debug=configuration.get('geral','debug')

		# database
		self.dbhost=configuration.get('mysql','dbhost')
		self.dbuser=configuration.get('mysql','dbuser')
		self.dbpwd=configuration.get('mysql','dbpwd')
		self.dbname=configuration.get('mysql','dbname')

		# asterisk
		self.asterisk_type=configuration.get('dbasterisk','asterisk_type')
		self.dbasterisk_type=configuration.get('dbasterisk','dbasterisk_type')
		self.dbasterisk_host=configuration.get('dbasterisk','dbasterisk_host')
		self.dbasterisk_user=configuration.get('dbasterisk','dbasterisk_user')
		self.dbasterisk_pwd=configuration.get('dbasterisk','dbasterisk_pwd')
		self.dbasterisk_name=configuration.get('dbasterisk','dbasterisk_name')
		self.dbasterisk_getsip=configuration.get('dbasterisk','dbasterisk_getsip')
		self.dbasterisk_getsipname=configuration.get('dbasterisk','dbasterisk_getsipname')

		# email
		#self.email_dest=configuration.get('email','email_dest')
		#self.email_send=configuration.get('email','email_send')
		#self.email_host=configuration.get('email','email_host')

