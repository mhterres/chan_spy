#!/usr/bin/python
# -*- coding: utf-8 -*-

# chanspy.py
# Administrador do ChanSpy
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-02-20
#

import web
from web import form
from dbmysql import DBMysql
from dbasterisk import AsteriskDB
from config_cs import Config

cfg=Config()
DB=DBMysql(cfg)
dbAsterisk=AsteriskDB(cfg)

urls = (
	'/', 'lista_monitores',
	'/lista/(.*)', 'lista_monitores',
	'/edit/(.*)', 'edita_monitores',
	'/save/', 'save_monitor',
	'/delete/(.*)', 'delete_monitor'
)

app = web.application(urls, globals())

class lista_monitores:
	def GET(self,anything="anything"):

		monitores=DB.search_monitores()

		ramais_monitores=[]

		if len(monitores)>0:

			for monitor in monitores:

				ramais=DB.search_ramais(monitor['monitor_id'])	

				ramais_monitorados=[]

				if len(ramais)>0:

					for ramal in ramais:

						ramais_monitorados.append(dict(ramal_id=ramal['ramal_id'],ramal=ramal['ramal']))

				ramais_monitores.append(dict(monitor_id=monitor['monitor_id'],ramal=monitor['ramal'],senha=monitor['senha'],nome=monitor['nome'],ramais=ramais_monitorados))

		data=dict(title="ChanSpy",subtitle="Lista de ramais monitores",monitores=ramais_monitores)
		lista=web.template.frender("html/lista.html")
		return lista(data)

class edita_monitores:

	def GET(self,monitor_id):

		sips=dbAsterisk.get_sips()

		if int(monitor_id)==0:

			subtitle="Novo ramal monitor"

			ramal=""
			nome=""
			senha=""
			ramais=sips
			ramaismon=[]
		else:

			subtitle="Edição do ramal monitor"

			data_ramal=DB.search_monitor(str(monitor_id))

			if len(data_ramal)>0:

				ramal=data_ramal['ramal']
				nome=data_ramal['nome']
				senha=data_ramal['senha']
				ramais=[]
				ramaismon=[]

				ramaism_db=DB.search_ramais(int(monitor_id))

				if len(ramaism_db)>0:

					r_m=[]

					for item in ramaism_db:

						ramaismon.append([item['ramal_id'],item['ramal']])
						r_m.append(item['ramal'])
	
					for sip in sips:

						if sip not in r_m:

							ramais.append(sip)

			else:
				ramal=""
				nome=""
				senha=""
				ramais=sips
				ramaismon=[]

		dados=dict(ramal=ramal,nome=nome,senha=senha,ramais=ramais,ramaismon=ramaismon)
			
		data=dict(title="ChanSpy",subtitle=subtitle,dados=dados)
		lista=web.template.frender("html/edit.html")
		return lista(data,monitor_id)


class save_monitor:

	def GET(self):

		form=web.input()

		monitor_id=form.monitor_id

		ramal=form.ramal
		nome=form.nome
		senha=form.senha

		if int(monitor_id)==0:

			if DB.VerificaRamalCadastrado(ramal):

				data=dict(title="ChanSpy",subtitle="Erro no cadastro do ramal %s" % ramal,ramal=ramal,erro=1)

				form=web.template.frender("html/save.html")
				return form(data)

		ramais_monitorados=form.ramais_monitorados.split(",")

		ramais=[]

		for ramal_monitorado in ramais_monitorados:

			if ramal_monitorado != "":

				nome_ramalmon=dbAsterisk.getNomeRamal(ramal_monitorado)

				ramais.append(dict(ramal=ramal_monitorado,nome=nome_ramalmon))

		dados=dict(monitor_id=monitor_id,ramal=ramal,nome=nome,senha=senha,ramais=ramais)

		if int(monitor_id)==0:

			DB.insert_monitor(dados)
		else:

			DB.update_monitor(dados)

		data=dict(title="ChanSpy",subtitle="Cadastro do ramal %s" % ramal,ramal=ramal,erro=0)

		form=web.template.frender("html/save.html")
		return form(data)


class delete_monitor:

	def GET(self,monitor_id):

		DB.delete_monitor(monitor_id)

		data=dict(title="ChanSpy",subtitle="Deleção de ramal")

		form=web.template.frender("html/delete.html")
		return form(data)

if __name__  == "__main__":
	app.run()

