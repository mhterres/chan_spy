-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 20/02/2015 às 21:01
-- Versão do servidor: 5.5.41-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Banco de dados: `chanspy`
--
CREATE DATABASE IF NOT EXISTS `chanspy` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `chanspy`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `monitores`
--
-- Criação: 20/02/2015 às 23:00
--

DROP TABLE IF EXISTS `monitores`;
CREATE TABLE IF NOT EXISTS `monitores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ramal` varchar(8) NOT NULL,
  `senha` varchar(4) NOT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ramal` (`ramal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `ramais`
--
-- Criação: 20/02/2015 às 23:01
--

DROP TABLE IF EXISTS `ramais`;
CREATE TABLE IF NOT EXISTS `ramais` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `monitor_id` bigint(20) NOT NULL,
  `ramal` varchar(8) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ouvinte_id` (`monitor_id`,`ramal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- RELACIONAMENTOS PARA TABELAS `ramais`:
--   `monitor_id`
--       `monitores` -> `id`
--

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `ramais`
--
ALTER TABLE `ramais`
  ADD CONSTRAINT `ramais_ibfk_1` FOREIGN KEY (`monitor_id`) REFERENCES `monitores` (`id`) ON DELETE CASCADE;



