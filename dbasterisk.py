#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		dbasterisk.py
		Classe para acesso ao DB do Asterisk
		2015/02/27
		Marcelo Hartmann Terres <mhterres@mundoopensource.com.br>
"""
import MySQLdb
import psycopg2
import psycopg2.extras

class AsteriskDB:

	def __init__(self,config):

		self.description = '%s %s Database' % (config.dbasterisk_type,config.asterisk_type)
		self.config=config

		if config.dbasterisk_type=="mysql":

			self.conn = MySQLdb.connect(config.dbasterisk_host,config.dbasterisk_user,config.dbasterisk_pwd,config.dbasterisk_name)

		else:

			self.dsn = 'dbname=%s host=%s user=%s password=%s' % (config.dbasterisk_name,config.dbasterisk_host,config.dbasterisk_user,config.dbasterisk_pwd)
			self.conn = psycopg2.connect(self.dsn)

	def get_sips(self):

		sql = self.config.dbasterisk_getsip

		cursor = self.conn.cursor()
		cursor.execute(sql)

		data=[]

		if cursor.rowcount>0:

			row = cursor.fetchone()

			while row is not None:

				data.append(row[0])
				row = cursor.fetchone()
			
		cursor.close()

		return data

	def getNomeRamal(self,ramal):

		sql = self.config.dbasterisk_getsipname % ramal

		cursor = self.conn.cursor()
		cursor.execute(sql)

		row = cursor.fetchone()

		nome=row[0]

		cursor.close()
			
		return nome


